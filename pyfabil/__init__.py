__author__ = 'Alessio Magro'
__version__ = '2.1.0'

from pyfabil.base.definitions import *
from pyfabil.boards.tpm_generic import TPMGeneric
from pyfabil.boards.tpm import TPM
