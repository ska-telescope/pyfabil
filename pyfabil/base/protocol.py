from builtins import object
class Protocol(object):
    """ Class representing a communication protocol with an FPGA board """

    def __init__(self, board):
        """ Class constructor """
        self._sock = None
        self._ip = None
        self._port = None
        self._board = board

    def create_connection(self, ip, port):
        """ Create connection
        :param ip: Remote IP
        :param port: Remote port """
        raise NotImplemented("{}.create_connection not implemented".format(self.__class__.__name__))

    def close_connection(self):
        """ Close connection """
        raise NotImplemented("{}.close_connection not implemented".format(self.__class__.__name__))

    def read_register(self, address, n, offset):
        """ Read register value from board
        :param address: Address to read from
        :param n: Number of values to read
        :param offset: Offset to read from """
        raise NotImplemented("{}.read_register not implemented".format(self.__class__.__name__))

    def write_register(self, address, values, offset):
        """ Write values to register
        :param address: Address to write to
        :param values: Values to write
        :param offset: Offset to write to"""
        raise NotImplemented("{}.write_register not implemented".format(self.__class__.__name__))

    def list_firmware(self):
        """ Get the list of downloaded firmware """
        raise NotImplemented("{}.list_firmware not implemented".format(self.__class__.__name__))

    def get_ip(self):
        return self._ip

    def get_port(self):
        return self._port
