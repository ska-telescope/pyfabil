from __future__ import print_function
from __future__ import absolute_import
from builtins import range
from builtins import object
import xml.etree.ElementTree as eTree
from .definitions import *
import logging


class MemoryMap(object):
    """ Class which represents a Memory Map"""

    def __init__(self):
        """ class constructor """
        self.register_list = {}

    def update_memory_map(self, xml_string, device, base_address):
        """ Update memory map
         :param xml_string: XML string to use to update memory map
         :param device: Device to use
         :param base_address: Base address to offset addresses in memory map """

        # Otherwise, parse XML file
        logging.debug("Updating Memory Map")

        # Parse string and get root
        tree = eTree.fromstring(xml_string)

        # Device name to append to registers
        device_name = self._get_device_name(device)

        # We are at the root of the XML file, we need to iterate through the
        # child nodes to access all FPGA and board memory maps
        for device_node in tree:

            # Get node type
            if device == Device.Board:
                current_type = RegisterType.BoardRegister
            else:
                current_type = RegisterType.FirmwareRegister

            # Get base address for device (if any)
            if 'address' in device_node.attrib and device_node.attrib['address'] != 0:
                device_address = int(device_node.attrib['address'], 16) + base_address
            else:
                device_address = base_address

            # Loop over device node attributes
            for k, v in device_node.attrib.items():
                # Process attributes
                if k in ['attribute', 'id']:
                    pass  # Already processed, skip
                elif k == 'module':
                    pass  # We need to load an additional file, ignore for now
                else:
                    pass  # Unsupported attribute, skip for now

            # Loop over blocks within the device (related to higher level components
            # in firmware or device groups for the board itself)
            for component_node in device_node:
                # New component detected, get component ID
                component_id = "{}.{}".format(device_name, component_node.attrib['id'])

                # Check if base address is specified
                component_address = 0
                if 'address' in component_node.attrib:
                    component_address = int(component_node.attrib['address'], 16)

                # Check if description is specified
                component_description = ""
                if 'description' in component_node.attrib:
                    component_description = component_node.attrib['description']

                # Create entry for memory map
                register_info = RegisterInfo(name=component_id,
                                             address=component_address + device_address,
                                             reg_type=RegisterType.Component,
                                             device=device,
                                             permission=Permission.Read,
                                             bitmask=0xFFFFFFFF,
                                             bits=32,
                                             shift=0,
                                             value=0,
                                             size=1,
                                             desc=component_description)
                self.register_list[component_id] = register_info

                # Note, additional attributes are ignored for now

                # Loop through list of registers for current component
                for register_node in component_node:

                    # New register detected, get register ID
                    register_id = "{}.{}".format(component_id, register_node.attrib['id'])

                    # Check if base address is specified
                    if 'address' in register_node.attrib:
                        register_address = int(register_node.attrib['address'], 16) + \
                                        device_address + component_address
                    else:
                        register_address = device_address + component_address

                    # Create RegisterInfo object and set defaults
                    register_info = RegisterInfo(name=register_id,
                                                 address=register_address,
                                                 reg_type=current_type,
                                                 device=device,
                                                 permission=Permission.Read,
                                                 bitmask=0xFFFFFFFF,
                                                 bits=32,
                                                 shift=0,
                                                 value=0,
                                                 size=1,
                                                 desc='')

                    # Loop over register attributes and update info
                    for k, v in register_node.attrib.items():
                        if k in ['id', 'address', 'mode', 'module']:
                            pass  # Already processed or no used at the moment

                        # Process bitmask
                        elif k == 'mask':
                            # Set register mask
                            register_info.bitmask = int(v, 16)

                            # Get number of bits and bit shifts
                            shift, bits = 0, 0
                            found = False
                            for i in range(32):
                                if ((register_info.bitmask >> i) & 0x00000001) == 1:
                                    bits += 1
                                    if not found:
                                        found = True
                                        register_info.shift = i

                            register_info.bits = bits

                        # Process persmission
                        elif k == 'permission':
                            if v == 'r':
                                register_info.permission = Permission.Read
                            elif v == 'w':
                                register_info.permission = Permission.Write
                            elif v in ['rw', 'wr']:
                                register_info.permission = Permission.ReadWrite
                            else:
                                pass  # Ignore unknown value

                        # Process size
                        elif k == 'size':
                            register_info.size = int(v)

                        # Process description
                        elif k == 'description':
                            register_info.desc = v

                        # Process tags
                        elif k == 'tags':
                            if v == 'sensor':
                                register_info.type = RegisterType.Sensor
                            elif v == 'fifo':
                                register_info.type = RegisterType.FifoRegister

                    # Add to memory map
                    self.register_list[register_id] = register_info

                    # Check whether register contains bitfields
                    for bit_node in register_node:

                        # New bitfield detected, get bitfield ID
                        bitfield_id = "{}.{}".format(register_id, bit_node.attrib['id'])

                        # Create RegisterInfo object and set defaults
                        register_info = RegisterInfo(name=bitfield_id,
                                                     address=register_address,
                                                     reg_type=current_type,
                                                     device=device,
                                                     permission=Permission.Read,
                                                     bitmask=0xFFFFFFFF,
                                                     bits=32,
                                                     shift=0,
                                                     value=0,
                                                     size=1,
                                                     desc='')

                        # Loop over bitfield attributes and update register info
                        for k, v in bit_node.attrib.items():
                            if k in ['id', 'address', 'mode', 'module']:
                                pass  # Already processed or no used at the moment

                            # Process bitmask
                            elif k == 'mask':
                                # Set register mask
                                register_info.bitmask = int(v, 16)

                                # Get number of bits and bit shifts
                                shift, bits = 0, 0
                                found = False
                                for i in range(32):
                                    if ((register_info.bitmask >> i) & 0x00000001) == 1:
                                        bits += 1
                                        if not found:
                                            found = True
                                            register_info.shift = i

                                register_info.bits = bits

                            # Process persmission
                            elif k == 'permission':
                                if v == 'r':
                                    register_info.permission = Permission.Read
                                elif v == 'w':
                                    register_info.permission = Permission.Write
                                elif v in ['rw', 'wr']:
                                    register_info.permission = Permission.ReadWrite
                                else:
                                    pass  # Ignore unknown value

                            # Process size
                            elif k == 'size':
                                register_info.size = int(v)

                            # Process description
                            elif k == 'description':
                                register_info.desc = v

                            # Process tags
                            elif k == 'tags':
                                if v == 'sensor':
                                    register_info.type = RegisterType.Sensor
                                elif v == 'fifo':
                                    register_info.type = RegisterType.FifoRegister

                        # Add to memory map
                        self.register_list[bitfield_id] = register_info

    def add_register_entry(self, device, register, address, offset):
        """ Add register entry
        :param device: Device to which the register belongs to
        :param register: Register name
        :param address: Register address
        :param offset: Offset """

        # Create new register info
        register_info = RegisterInfo(name=register,
                                     address=address,
                                     reg_type=RegisterType.BoardRegister,
                                     device=device,
                                     permission=Permission.ReadWrite,
                                     bitmask=0xFFFFFFFF,
                                     bits=32,
                                     shift=0,
                                     value=0,
                                     size=offset,
                                     desc='')

        # Insert new or update existing entry
        self.register_list[device][register] = register_info

    def clear(self):
        """ Reset memory map """
        self.register_list = {}

    def has_register(self, register):
        """ Check if memory map contains specified register
        :param register: Register to check """
        return register in list(self.register_list.keys())

    @staticmethod
    def _get_device_name(device):
        """ Return string representation of device """
        if device == Device.Board:
            return "board"
        elif device == Device.FPGA_1:
            return "fpga1"
        elif device == Device.FPGA_2:
            return "fpga2"
        elif device == Device.FPGA_3:
            return "fpga3"
        elif device == Device.FPGA_4:
            return "fpga4"
        elif device == Device.FPGA_5:
            return "fpga5"
        elif device == Device.FPGA_6:
            return "fpga6"
        elif device == Device.FPGA_7:
            return "fpga7"
        elif device == Device.FPGA_8:
            return "fpga8"

    def __getitem__(self, key):
        """ Override __getitem__, return register information for specified register """

        # If no memory map exists, return None
        if len(self.register_list) == 0:
            raise LibraryError("Cannot get register from uninitialised memory map")

        if key not in list(self.register_list.keys()):
            raise LibraryError("Specified register {}, does not exist in memory map".format(key))

        # Register found, return information
        return self.register_list[key]

    def __len__(self):
        return len(self.register_list)


if __name__ == "__main__":
    test = MemoryMap()
    input_string = open("/home/lessju/Desktop/tpm_test_output.xml").read()
    test.update_memory_map(input_string, Device.FPGA_1, 0x0)
    print(test['fpga1.f2f_1.test_error'])
