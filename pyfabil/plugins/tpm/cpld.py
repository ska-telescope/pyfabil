from __future__ import division
from builtins import str
from builtins import hex
from builtins import range
__author__ = 'lessju'

from pyfabil.plugins.firmwareblock import FirmwareBlock
from pyfabil.base.definitions import *
from pyfabil.base.utils import *
from time import sleep
import logging
import os


class TpmCpld(FirmwareBlock):
    """ TpmCpld plugin """

    @compatibleboards(BoardMake.TpmBoard)
    @friendlyname('tpm_cpld')
    @maxinstances(1)
    def __init__(self, board, **kwargs):
        """ TpmCpld initialiser
        :param board: Pointer to board instance
        """
        self._cpld_fw_start_add = 0x10000
        self._cpld_fw_date_add = 0x30000000
        self._max_bitstream_size=0x10000*2
        
        super(TpmCpld, self).__init__(board)

        self.board_type = kwargs.get('board_type', 'XTPM')

    #######################################################################################
    def get_version(self):
        version = hex(self.board[self._cpld_fw_date_add])
        return version

    def cpld_flash_read(self, bitfile="cpld_dump.bit"):
        """ Read CPLD FLASH """
        return self.board.tpm_progflash.firmwareRead(0, self._cpld_fw_start_add, self._max_bitstream_size, bitfile)

    def cpld_flash_write(self, bitfile):
        """ Write bitfile to CPLD FLASH """
        logging.info("Writing {} to CPLD flash".format(bitfile))
        return self.board.tpm_progflash.firmwareProgram(0, bitfile, self._cpld_fw_start_add)

    def cpld_efb_wr(self, dat):
        raise NotImplementedError("TpmCpld.cpld_efb_wr not implemented")

    def eep_rd8(self, offset):
        """ Read 8-bit value from EEP ROM """
        i2c_data = offset & 0xFF
        return self.board.tpm_i2c.read(i2c_add=0xA0,
                                       nof_wr_byte=1,
                                       nof_rd_byte=1,
                                       wr_data=i2c_data)

    def eep_wr8(self, offset, data):
        """ Write 8-bit value to EEP ROM """
        i2c_data = (data & 0xFF) << 8 + (offset & 0xFF)
        self.board.tpm_i2c.read(i2c_add=0xA0,
                                nof_wr_byte=2,
                                nof_rd_byte=0,
                                wr_data=i2c_data)
        return

    def eep_rd32(self, offset):
        """ Read 32-bit value from EEP ROM """
        rd = 0
        for n in range(4):
            rd = rd << 8
            rd = rd | self.eep_rd8(offset + n)
        return rd

    def eep_wr32(self, offset, data):
        """ Write 32-bit value to EEP ROM """
        for n in range(4):
            self.eep_wr8(offset + n, (data >> 8 * (3 - n)) & 0xFF)
            return


    ##################### Superclass method implementations ###############################


    def initialise(self):
        """ Initialise TpmCpld """
        logging.info("TpmCpld has been initialised")
        return True

    def status_check(self):
        """ Perform status check
        :return: Status
        """
        logging.info("TpmCpld : Checking status")
        return Status.OK

    def clean_up(self):
        """ Perform cleanup
        :return: Success
        """
        logging.info("TpmCpld : Cleaning up")
        return True
