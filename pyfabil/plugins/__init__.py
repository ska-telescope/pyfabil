__author__ = 'Alessio Magro'

# Helper to reduces import names

# Plugin Superclass
from pyfabil.plugins.firmwareblock import FirmwareBlock

# TPM plugins
from pyfabil.plugins.tpm.firmware_information import TpmFirmwareInformation
from pyfabil.plugins.tpm.pattern_generator import TpmPatternGenerator
from pyfabil.plugins.tpm.test_generator import TpmTestGenerator
from pyfabil.plugins.tpm.antenna_buffer import AntennaBuffer
from pyfabil.plugins.tpm.adc_power_meter import AdcPowerMeter
from pyfabil.plugins.tpm.spead_tx_gen import SpeadTxGen
from pyfabil.plugins.tpm.forty_g_xg_core import TpmFortyGCoreXg
from pyfabil.plugins.tpm.beamf_fd import BeamfFD
from pyfabil.plugins.tpm.multiple_channel_tx import MultipleChannelTx
from pyfabil.plugins.tpm.station_beamf import StationBeamformer
from pyfabil.plugins.tpm.integrator import TpmIntegrator
from pyfabil.plugins.tpm.clock_monitor import TpmClockmon
from pyfabil.plugins.tpm.fpga import TpmFpga
from pyfabil.plugins.tpm.jesd import TpmJesd
from pyfabil.plugins.tpm.progflash import TpmProgFlash
from pyfabil.plugins.tpm.adc_ad9695 import TpmAdc9695
from pyfabil.plugins.tpm.f2f_aurora import TpmFpga2FpgaAurora
from pyfabil.plugins.tpm.cpld import TpmCpld
from pyfabil.plugins.tpm.pll import TpmPll
from pyfabil.plugins.tpm.mcu import TpmMcu
from pyfabil.plugins.tpm.eep import TpmEEP
from pyfabil.plugins.tpm.preadu import TpmPreAdu
from pyfabil.plugins.tpm.sysmon import TpmSysmon
from pyfabil.plugins.tpm.qsfp_adapter import TpmQSFPAdapter
