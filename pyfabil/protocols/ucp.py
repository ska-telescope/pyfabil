from __future__ import print_function
from __future__ import division
from builtins import bytes
from builtins import hex
from builtins import range
import array
import logging
import random
import socket
import struct
import time
from math import ceil

from enum import Enum

from pyfabil import BoardMake, LibraryError
from pyfabil.base.protocol import Protocol


class UCP(Protocol):
    """ Class implementing the UCP communication protocol """

    # UCP OPCODE definitions
    class OPCODE(Enum):
        """ Device enumeration """
        READ = 0x01
        WRITE = 0x02
        BITWISE_AND = 0x03
        BITWISE_OR = 0x04
        FLASH_WRITE = 0x06
        FLASH_READ = 0x07
        FLASH_ERASE = 0x08
        FIFO_READ = 0x09
        FIFO_WRITE = 0x0A
        BIT_WRITE = 0x0B
        RESET_BOARD = 0x11
        PERIODIC_UPDATE = 0x12
        ASYNC_UPDATE = 0x13
        CANCEL_UPDATE = 0x14
        OVERLAPPED_WRITE = 40
        WAIT_FOR_PPS = 0xFFFFFFFF

    def __init__(self, board):
        """ UCP class constructor """
        super(UCP, self).__init__(board)

        # Initialise sequence number
        self._sequence_number = int(random.random() * 100000)

        # Maximum payload size
        self._max_values_in_payload = 256

        # Define socket properties
        self._socket_timeout = 1
        self._socket_buffer_size = 1024 * 1024
        self._max_retries = 3

    def create_connection(self, ip, port, src_ip = None):
        """ Create connection
        :param ip: Remote IP
        :param port: Remote port """

        # Create and configure socket
        try:
            self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            if src_ip is not None:
                logging.warning("UCP: bind socket: {}".format(src_ip))
                self._sock.bind((src_ip, 0))
            self._sock.settimeout(self._socket_timeout)
            self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, self._socket_buffer_size)
            if ip == "255.255.255.255":
                self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        except Exception as e:
            logging.debug("UCP: Could not connect to board with IP {}".format(ip))
            return False

        # Save connection information
        self._ip = ip
        self._port = port

        return True

    def close_connection(self):
        """ Close socket connection """
        self._sock.close()

    def read_register(self, address, n, offset):
        """ Read register value from board
        :param address: Address to read from
        :param n: Number of values to read
        :param offset: Offset to read from """

        # Check if we need to split this request into multiple packets
        num_packets = (n // self._max_values_in_payload) + 1

        # List holding values
        to_return = []

        # Issue requests one by one
        for i in range(num_packets):

            # Increment sequence number
            self._sequence_number += 1

            # Number of words for current request
            num_values = n - i * self._max_values_in_payload if (i == num_packets - 1) else self._max_values_in_payload

            # New address
            current_address = address + (offset + i * self._max_values_in_payload) * 4

            # Generate request
            pkt = self._create_request(self.OPCODE.READ.value, num_values, current_address)

            # Send packet
            data, retries = None, 0
            while retries < self._max_retries:
                # Send request
                if self._send_packet(pkt):
                    # Get reply
                    data = self._receive_packet(10240)
                    if data:
                        # Sent and received packet, break from loop
                        break
                    else:
                        logging.warning("UCP::read. Failed to receive response. Retrying {}".format(retries + 1))
                        retries += 1
                else:
                    logging.warning("UCP::read. Failed to send request. Retrying {}".format(retries + 1))
                    retries += 1

            # Check if operation was successful
            if retries == self._max_retries:
                # Failure, return
                logging.error("UCP::read. Failed operation")
                raise LibraryError("UCP::read. Failed operation")

            # Successfully received reply, add values to list
            to_return += self._check_reply(data, address=current_address, num_values=num_values, is_read=True)

        # All done, return data
        return to_return

    def write_register(self, address, values, offset=0, retry=True):
        """ Write values to register
        :param address: Address to write to
        :param values: Values to write
        :param offset: Offset to write to
        :param retry: Enable retries of write operation"""

        # Check if we need to split this request into multiple packets
        n = len(values)
        num_packets = int(ceil(float(n) / float(self._max_values_in_payload)))

        # Issue requests one by one
        for i in range(num_packets):

            # Increment sequence number
            self._sequence_number += 1

            # Number of words for current request
            num_values = n - i * self._max_values_in_payload if (i == num_packets - 1) else self._max_values_in_payload

            # New address
            current_address = address + (offset + i * self._max_values_in_payload) * 4

            # Generate request
            pkt = self._create_request(self.OPCODE.WRITE.value, num_values, current_address,
                                       values[i * self._max_values_in_payload: i * self._max_values_in_payload + num_values])

            # Send packet
            data, retries = None, 0
            while retries < self._max_retries:
                # Send request
                if self._send_packet(pkt):
                    # Get reply
                    data = self._receive_packet(10240)

                    if retry == False:
                        return

                    if data:
                        break
                    else:
                        # Something went wrong. If we are dealing with a TPM, the check whether
                        # request was processing but we lost the reply
                        if self._board == BoardMake.TpmBoard:
                            if self._last_processed_psn(self._sequence_number):
                                # Request was processed by the board, all good
                                break

                        # Otherwise, retry
                        logging.warning("UCP::write. Failed to receive ack. Retrying {}".format(retries + 1))
                        retries += 1
                else:
                    logging.warning("UCP::write. Failed to send request. Retrying {}".format(retries + 1))
                    retries += 1

            # Check if operation was successful
            if retries == self._max_retries:
                # Failure, return
                logging.error("UCP::write. Failed operation. Attempted " + str(self._max_retries) + " retries.")
                raise LibraryError("UCP::write. Failed operation. Attempted " + str(self._max_retries) + " retries.")

            # Process returned data
            self._check_reply(data, address=current_address, is_read=False)

    def select_map_program(self, bitfile_data, fifo_addr=None, ucp_smap_write=True, lock_address=None, lock_value=None,
                           relock_time=0.2):
        """ Program the board using select map
        :param values: Values to write """

        if ucp_smap_write:
            ucp_opcode = self.OPCODE.OVERLAPPED_WRITE.value
        else:
            ucp_opcode = self.OPCODE.WRITE.value

        # Check if we need to split this request into multiple packets
        n = len(bitfile_data) // 4
        num_packets = int(ceil(float(n) / float(self._max_values_in_payload)))

        # Send the first packet
        num_values = n if n < self._max_values_in_payload else self._max_values_in_payload
        values = list(struct.unpack_from('I' * num_values, bitfile_data[:num_values * 4]))
        pkt = self._create_request(ucp_opcode, num_values, address=fifo_addr, values=values)

        # Temporary FIX, avoid overlap
        # Send packet
        #data, retries = None, 0
        #while retries < self._max_retries:
        #    # Send request
        #    if self._send_packet(pkt):
        #        break
        #    else:
        #        retries += 1

        data, retries = None, 0
        while retries < self._max_retries:
            # Send request
            if self._send_packet(pkt):
                # Get reply
                data = self._receive_packet(10240)
                if data:
                    break
                else:
                    retries += 1
            else:
                retries += 1

        # Temporary FIX, avoid overlap
        self._check_reply(data, sequence_number=self._sequence_number)
        start = time.time()
        # Issue next requests one by one
        for i in range(1, num_packets):
            # reload lock reg if request
            if lock_address is not None:
                end = time.time()
                if end - start > relock_time:
                    self.write_register(lock_address, [lock_value])
                    start = end
            # Increment sequence number
            self._sequence_number += 1

            # Number of words for current request
            num_values = n - i * self._max_values_in_payload if (i == num_packets - 1) else self._max_values_in_payload
            values = list(struct.unpack_from('I' * num_values,
                                             bitfile_data[i * self._max_values_in_payload * 4:
                                                          i * self._max_values_in_payload * 4 + num_values * 4]))
            # Generate request
            pkt = self._create_request(ucp_opcode, num_values, address=fifo_addr, values=values)

            # Send packet
            data, retries = None, 0
            while retries < self._max_retries:
                # Send request
                if self._send_packet(pkt):
                    # Get reply
                    data = self._receive_packet(10240)
                    if data:
                        break
                    else:
                        retries += 1
                else:
                    retries += 1

            # Check if operation was successful
            if retries == self._max_retries:
                # Failure, return
                raise LibraryError("UCP::select_map_write. Failed to send request")

            # Process returned data
            # self._check_reply(data, sequence_number=self._sequence_number-1)
            # Temporary FIX, avoid ovelap
            self._check_reply(data, sequence_number=self._sequence_number)

        # Receive last ack
        #data, retries = None, 0
        #while retries < self._max_retries:
        #    data = self._receive_packet(10240)
        #    if data:
        #        break
        #    else:
        #        retries += 1

        # Check if operation was successful
        #if retries == self._max_retries:
        #    # Failure, return
        #    raise LibraryError("UCP::select_map_write. Failed to send request")

        # Process returned data
        #self._check_reply(data)

    def _create_request(self, opcode, n, address=None, values=None):
        """ Creates UCP request
         :param opcode: UCP OPCODE
         :param n: Numbre of operands
         :param address: Address where operation will be performed
         :param values: Values to write, if any"""

        pkt = array.array('I')
        pkt.append(self._sequence_number)
        pkt.append(opcode)
        pkt.append(n)

        if address is not None:
            pkt.append(address)

        if values is not None:
            pkt.extend(values)

        # Return packed request
        return bytes(pkt)

    def _check_reply(self, data, address=None, sequence_number=None, num_values=None, is_read=False):
        """ Check whether packet was processed correctly
        :param data: Data to check
        :param psn: Packet sequence number
        :param address: Address """

        if sequence_number is None:
            sequence_number = self._sequence_number

        # Check number of bytes read
        if num_values is not None and len(data) / 4 < num_values:
            raise LibraryError("UCP::read. Failed to receive reply. Expecting %d bytes, received %d bytes" % (num_values * 4, len(data)))
        
        # Process returned data
        data = bytes(data)
        psn = struct.unpack('I', data[0:4])[0]

        # Check if request was successful on board
        if psn != sequence_number:
            raise LibraryError("UCP::read. Command failed on board on address %s - PSN error: %d, %d" % (hex(address), psn, sequence_number))

        if address is not None:
            received_address = struct.unpack('I', data[4:8])[0]
            if received_address != address:
                operation = "read" if is_read == True else "write"
                raise LibraryError("UCP::" + operation + ". Command failed on board. Requested address " + hex(address) + " received address " + hex(received_address))

        if num_values is not None:
            return struct.unpack('I' * num_values, data[8:])

    def _receive_packet(self, max_length):
        """ Receive a packet of up to max_length"""
        try:
            data, _ = self._sock.recvfrom(max_length)
        except:
            return None

        return data

    def _send_packet(self, message):
        """ Send packet """
        ret = False
        try:
            if self._sock.sendto(message, (self._ip, self._port)) != -1:
                ret = True
        except:
            pass

        return ret

    def _last_processed_psn(self, psn):
        """ Check if a request was processed by reading the last processed PSN """

        logging.info("UCP::last_process_PSN. Trying to recover command.")

        # Sequence number for checking
        seqno = 0xBEEF

        # Create request
        pkt = array.array('I')
        pkt.append(seqno)             # PSN
        pkt.append(self.OPCODE.READ.value)  # OPCODE
        pkt.append(1)                 # Number of operands
        pkt.append(0x30000004)        # Address
        pkt = bytes(pkt)

        # Send packet
        data = None
        retries = 0
        while retries < self._max_retries:
            # Send request
            if self._send_packet(pkt):
                data = self._receive_packet(10240)
                if data:
                    # Sent and received packet, break from loop
                    break
                else:
                    retries += 1
            else:
                retries += 1

        # Check if operation was successful
        if retries == self._max_retries:
            # Failure, return
            logging.error("UCP::last_process_PSN. Failed to send request")
            return False

        # Process returned data
        data = bytes(data)
        local_psn = struct.unpack('I', data[0:4])[0]
        address = struct.unpack('I', data[4:8])[0]

        # Check if request was successful on board
        if local_psn != seqno or address != 0x30000004:
            logging.error("UCP::last_process_PSN: Command failed on board")

        # Check whether PSNs match
        if local_psn == psn:
            return True
        else:

            return False


if __name__ == "__main__":
    ucp = UCP(BoardMake.TpmBoard)
    ucp.create_connection("10.0.10.3", 10000)
    print(ucp.read_register(0x0, 1, 0))
