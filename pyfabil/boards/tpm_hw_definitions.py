# Hardware Version
TPM_HW_REV_1_2 = 0x010200
TPM_HW_REV_1_5 = 0x010500
TPM_HW_REV_1_6 = 0x010600
TPM_HW_REV_2_0 = 0x020000

# Hardcoded Offsets in CPLD memory
CPLD_MAGIC_OFFSET = 0x80000000
CPLD_XML_OFFSET = 0x80000004
CPLD_VERSION_OFFSET = 0x30000000
CPLD_MCU_VERSION_OFFSET = 0x90000000

# CPLD Constants
CPLD_MAGIC_TPM_V1_2 = 0xF1233210
CPLD_MAGIC_TPM_V1_5 = 0xF1233215

# Hardcoded Offset is FPGA memory
FPGA_MAGIC_FPGA0_OFFSET = 0x00000004
FPGA_MAGIC_FPGA1_OFFSET = 0x10000004
# FPGA constant
FPGA_MAGIC = 0xA1CE55AD

# Specific CPLD and MCU versions
# Blacklisted firmware versions
CPLD_FW_VER_BLACKLIST_FOR_XML_SUPPORT_TPM_V1_2 = 0x31116B0

# Required for FPGA firmware download using normal UCP accesses
CPLD_FW_VER_REQUIRED_FOR_FPGA_DOWNLOAD_TPM_V1_2 = 0x17041801
CPLD_FW_VER_REQUIRED_FOR_FPGA_DOWNLOAD_TPM_V1_5 = 0x0

# Required for FPGA firmware download using SMAP opcode in UCP for both TPM 1.2 and 1.6
CPLD_FW_VER_REQUIRED_FOR_SMAP_DOWNLOAD = 0x18050200

# Required to support shadow board temperature register, automatically updated by CPLD in TPM 1.2
CPLD_FW_VER_REQUIRED_FOR_SHADOW_TEMPERATURE_TPM_V1_2 = 0x19121600

# Required to support I2C command ack to avoid conflict with automatic temperature reading on TPM 1.2
CPLD_FW_VER_REQUIRED_FOR_I2C_CMD_ACK_TPM_V1_2 = 0x23041900

# Bug corrections on I2C bus contention on TPM 1.6
CPLD_FW_VER_LOCK_I2C_CHANGE_TPM_V1_5 = 0x22032409
MCU_FW_VER_LOCK_I2C_CHANGE_TPM_V1_5 = 0xB0000118
MCU_FW_VER_ERROR_NO_MCU = 0x00000000

# FPGA Firmware exception with support for 800 MHz clock from PLL only for TPM 1.6s
FPGA_FW_800MHZ_EXCEPTION_TPM_V1_5 = 0x1021822








