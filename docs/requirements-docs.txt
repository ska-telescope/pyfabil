--index-url https://pypi.org/simple
--extra-index-url https://artefact.skao.int/repository/pypi-internal/simple
future
numpy
ska-ser-sphinx-theme>=0.1.1
recommonmark