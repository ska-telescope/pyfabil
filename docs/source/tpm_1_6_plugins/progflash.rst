progflash
==========

Overview
---------

CPLD and FPGA SPI Flash bitfile storage/access

Python Class & Methods Index
-----------------------------

.. automodule:: pyfabil.plugins.tpm_1_6.progflash
    :members:
    :undoc-members: