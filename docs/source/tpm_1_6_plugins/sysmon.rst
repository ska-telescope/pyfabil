sysmon
========

Overview
---------

configure system monitoring and get system monitoring voltages/currents.

Python Class & Methods Index
--------------------------------

.. automodule:: pyfabil.plugins.tpm_1_6.sysmon
    :members:
    :undoc-members: