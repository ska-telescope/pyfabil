cpld
========

Overview
---------

read/ write either a cpld bitstream or to a eep rom. write to cpld EFB.

Python Class & Methods Index
--------------------------------

.. automodule:: pyfabil.plugins.tpm_1_6.cpld
    :members:
    :undoc-members: