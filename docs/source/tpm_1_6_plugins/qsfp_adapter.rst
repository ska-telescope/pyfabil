qsfp_adapter
============

Overview
---------

configure and or get status of the 40g qsfp adapter for the tpm 1.6.

Python Class & Methods Index
------------------------------

.. automodule:: pyfabil.plugins.tpm_1_6.qsfp_adapter
    :members:
    :undoc-members: