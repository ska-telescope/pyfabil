adc_ad9695
==========

Overview
----------

Configure the JESD204B settings on the FPGA, that receives data from the ADC.

Python Class & Methods Index
------------------------------

.. automodule:: pyfabil.plugins.tpm_1_6.adc_ad9695
    :members:
    :undoc-members: