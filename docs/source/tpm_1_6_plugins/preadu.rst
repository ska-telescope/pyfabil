preadu
========

Overview
---------

initializes the preADU and controls the preADU attenuation.

Python Class & Methods Index
--------------------------------

.. automodule:: pyfabil.plugins.tpm_1_6.preadu
    :members:
    :undoc-members: