f2f_aurora
===========

Overview
----------

configure the fpga 2 fpga interface that uses the xilinx aurora ip.

Python Class & Methods Index
------------------------------

.. automodule:: pyfabil.plugins.tpm_1_6.f2f_aurora
    :members:
    :undoc-members: