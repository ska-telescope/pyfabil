mcu
====

Overview
----------

Microcontoller unit, arm processor, reponsible for reading monitoring points

Python Class & Methods Index
-----------------------------

.. automodule:: pyfabil.plugins.tpm_1_6.mcu
    :members:
    :undoc-members: