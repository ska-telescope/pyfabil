firmware_information
=====================

Overview
---------

gets or updates information about the firmware.

Python Class & Methods Index
-----------------------------

.. automodule:: pyfabil.plugins.tpm.firmware_information
    :members:
    :undoc-members: