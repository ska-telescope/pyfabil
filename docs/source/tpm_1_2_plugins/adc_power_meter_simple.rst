adc_power_meter_simple
========================

Overview
-----------

Simplifed version adc power meter.

Python Class & Methods Index
-------------------------------

.. automodule:: pyfabil.plugins.tpm.adc_power_meter_simple
    :members:
    :undoc-members: