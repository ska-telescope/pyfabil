integrator
============

Overview
---------

configures the integrated version of the LMC.

Python Class & Methods Index
-----------------------------

.. automodule:: pyfabil.plugins.tpm.integrator
    :members:
    :undoc-members: