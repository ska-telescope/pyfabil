fpga
=========

Overview
----------

configure mmcm on the FPGA, fpga resets, start/stop FPGA acquisition and data downloading through 1Gbit Ethernet and ADC clk.

Python Class & Methods Index
--------------------------------

.. automodule:: pyfabil.plugins.tpm.fpga
    :members:
    :undoc-members: