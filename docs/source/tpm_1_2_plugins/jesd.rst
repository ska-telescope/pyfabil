jesd
=======

Overview
-----------

configure the JESD settings on the FPGA, that receives data from the ADC

Python Class & Methods Index
------------------------------

.. automodule:: pyfabil.plugins.tpm.jesd
    :members:
    :undoc-members: