ten_g_xg_core
==============

Overview
----------

configures the 10g version of the udp core (not used by default, only if generic g_xg_eth_use_40g_core = false)

Python Class & Methods Index
-------------------------------

.. automodule:: pyfabil.plugins.tpm.ten_g_xg_core
    :members:
    :undoc-members: