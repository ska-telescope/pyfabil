adc
=======


Overview
-------------

Controls the ADC chip on the TPM.

Python Class & Methods Index
------------------------------

.. automodule:: pyfabil.plugins.tpm.adc
    :members:
    :undoc-members: