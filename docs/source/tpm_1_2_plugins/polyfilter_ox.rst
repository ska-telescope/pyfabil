polyfilter_ox
==============

Overview
-----------

configure the polyfilter. get filter param, available windows and window values.

Python Class & Methods Index
-----------------------------

.. automodule:: pyfabil.plugins.tpm.polyfilter_ox
    :members:
    :undoc-members: