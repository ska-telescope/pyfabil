cpld
=======

Overview
---------

read/ write either a cpld bitstream or to a eep rom. write to cpld EFB.

Python Class & Methods Index
------------------------------

.. automodule:: pyfabil.plugins.tpm.cpld
    :members:
    :undoc-members: