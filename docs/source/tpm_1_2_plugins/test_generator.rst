test_generator
===============

Overview
----------

configuration of the test signal generator, can generate ADC data.

Python Class & Methods Index
-------------------------------

.. automodule:: pyfabil.plugins.tpm.test_generator
    :members:
    :undoc-members: