f2f
=====

Overview
----------

configurations to do with the fpga 2 fpga comunication. e.g. set direction, run test, check pll lock status

Python Class & Methods Index
-------------------------------

.. automodule:: pyfabil.plugins.tpm.f2f
    :members:
    :undoc-members: