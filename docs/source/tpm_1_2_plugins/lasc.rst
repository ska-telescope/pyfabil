lasc
======

Overview
-----------

same job as MCU but in TPM 1.2, reponsible for reading monitoring points

Python Class & Methods Index
-------------------------------

.. automodule:: pyfabil.plugins.tpm.lasc
    :members:
    :undoc-members: