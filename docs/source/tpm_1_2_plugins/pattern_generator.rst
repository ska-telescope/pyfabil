pattern_generator
====================

Overview
-----------

can produce pattern data for jesd (ADC), and for the LMC channelizer, beamformer, intergrated channelizer, intergrated beamformer data

Python Class & Methods Index
------------------------------

.. automodule:: pyfabil.plugins.tpm.pattern_generator
    :members:
    :undoc-members: