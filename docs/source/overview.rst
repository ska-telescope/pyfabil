
Overview
===================================

PyFABIL is a python package called Python FPGA Board Interfacing Layer, that was designed to allow communication with specific FPGA boards.

This is achieved by having a software representation of an FPGA board that includes:

    * The network protocol to communicate with the board
    * A memory map that tells the software what registers, FIFOs and memory areas are exposed by the board
    * Information of additional devices on the board (such as SPI devices


Network Protocol
----------------
For the TPM the protocol to communicate with the board is UCP (with reliability) encapsulated in UDP. 


Memory Map
----------
With regards to the TPM, a memory map is composed by:

    * CPLD memory map, consisting of a compressed XML file stored in a CPLD block RAM, defining registers implemented in the CPLD
    * FPGA memory map, consisting of a compressed XML file stored in a FPGA block RAM, defining registers implemented in the FPGA
    * SPI description, consisting of a compressed XML file stored in a FPGA block RAM, describing the SPI devices on the TPM, such as PLL and ADCs

The XML memory map files contain the mapping between registers names and physical addresses. 
PyFABIL reads the XML memory maps from the TPM at runtime and provide translation between register names and physical addresses, 
so that registers can be addressed by name by software.

The FPGA XML files are generated automatically during the firmware compilation process and embedded into the compiled bitstream. 
This means that any change to the memory map implemented in the FPGA firmware is always propagated to the software. 
Moving existing registers to different physical addresses will also be transparent for the software. 

PyFABIL structure
------------------

This is the structure of PyFABIL:

.. image:: pyfabil_struct.png


FPGA board is the base class that implements the generic functionality (plugin loading, memory map interface, protocol interface).

Separate classes implement board-specific functionality by adding or overriding FPGA Board functions, using FPGA board as a parent class.

For TPM board-specific, functionality include programming and erasing the FPGAs, extracting the memory map from the programmed bitfile, 
loading the board hardware (like the ADCs), updating the CPLD, calibrate communication between FPGAs and CPLD.

The specific board classes implement plugins. For more details on plugins see :doc:`plugins`.

Build the documentation
-----------------------

To build the readthedocs, for pyfabil, create a python virtual environment at least python 3.8.1 and then goto the docs folder and run

.. code-block:: console

    pip install -r requirements-docs.txt

Than to build the docs run

.. code-block:: console

    make html 