Plugins
===================================

The functional behaviour of a FPGA board changes depending on the programmed firmware. Also, the hardware components of the board may change with different iterations.
A dynamic system is required to support different firmware, firmware upgrades and hardware upgrades and configurations.
A plugin based approach was adopted to allow compartmentilzation and encapsulation.
Plugins can be associated with a specific hardware component (such as ADC, PLL, FPGA, ETH ...) or firmware feature (such as channeliser and beamformer).
Plugins inherit an abstract plugin class which gives access to the board memory map and has abstract functions for initialization, status checking and cleanup.


In each plugin class, additional attributes can also be defined through decorators, these include

    * Friendly name, used to refer to a plugin instance
    * Compatible boards, this will be checked during plugin loading to be compatible with the board in use
    * Maximum number of instances, this will be checked during plugin loading, instantiating a number of plugins of that type larger than the defined maximum will generate an error
    * Firmware design association with major and minor version numbers, this will be checked during plugin loading, making sure that the current FPGA firmware is compatible with the plugin



Plugins from previous versions of the board are inherited, but can be overridden.

TPM 1.2 Plugins
------------------------------------

.. toctree::
   :maxdepth: 1
   
   tpm_1_2_plugins/ada
   tpm_1_2_plugins/adc
   tpm_1_2_plugins/adc_power_meter
   tpm_1_2_plugins/adc_power_meter_simple
   tpm_1_2_plugins/antenna_buffer
   tpm_1_2_plugins/beamf_fd
   tpm_1_2_plugins/beamf_simple
   tpm_1_2_plugins/clock_monitor
   tpm_1_2_plugins/cpld
   tpm_1_2_plugins/f2f
   tpm_1_2_plugins/firmware_information
   tpm_1_2_plugins/forty_g_xg_core
   tpm_1_2_plugins/fpga
   tpm_1_2_plugins/i2c
   tpm_1_2_plugins/integrator
   tpm_1_2_plugins/jesd
   tpm_1_2_plugins/lasc
   tpm_1_2_plugins/multiple_channel_tx
   tpm_1_2_plugins/pattern_generator
   tpm_1_2_plugins/pll
   tpm_1_2_plugins/polyfilter_ox
   tpm_1_2_plugins/preadu
   tpm_1_2_plugins/side_channel
   tpm_1_2_plugins/spead_tx_gen
   tpm_1_2_plugins/station_beamf
   tpm_1_2_plugins/sysmon
   tpm_1_2_plugins/ten_g_core
   tpm_1_2_plugins/ten_g_xg_core
   tpm_1_2_plugins/test_generator
   

TPM 1.6 Plugins
------------------------------------

.. toctree::
   :maxdepth: 1

   tpm_1_6_plugins/adc_ad9695
   tpm_1_6_plugins/eep
   tpm_1_6_plugins/f2f_aurora
   tpm_1_6_plugins/f2f_gt
   tpm_1_6_plugins/mcu
   tpm_1_6_plugins/progflash
   tpm_1_6_plugins/qsfp_adapter
   tpm_1_6_plugins/preadu
   tpm_1_6_plugins/cpld
   tpm_1_6_plugins/pll
   tpm_1_6_plugins/sysmon
   
