# Version History

## 2.1.0
* [MCCS-2148] Implement antenna buffer software plugin interface

## 2.0.1
* [MCCS-2163] Added support for XML memory map designs not named 'tpm_test'

## 2.0.0
* [MCCS-2164] Remove references to TPM 1.2 in the AAVS System and PyFABIL repository

## 1.5.0
* [MCCS-1556] Added station beam plugin methods for flagging

## 1.4.1
* [MCCS-2328] Add method to Enable/Disable all lanes in jesd

## 1.4.0
* [MCCS-2025] updated + added methods to monitor and control broadband RFI counters

## 1.3.3
* [MCCS-2300] Add methods to pattern generator
* [SKB-478] Update docstring
* [MCCS-2281] Pinned Numpy to 1.24.4
* [MCCS-2282] Support for NumPy 2.X

## 1.3.2
* [MCCS-2229] Correct Frequency Beam Table Assignment Masking in PyFABIL
* [SKB-426] Removed unused voltages VM_ADA0 and VM_ADA1

## 1.3.1
* [SPRTS-230] 200 MHz is zero in grafana integrated power (bandpass) plots

## 1.3.0
* [SP-3800] Complete the implementation of ECP-230134, updating SPS-CBF SPEAD time fields
